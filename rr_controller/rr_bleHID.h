
#include <BLEDevice.h> //Libraries necessary for program
#include <BLEUtils.h>
#include <BLEServer.h>
#include "BLE2902.h"
#include "BLEHIDDevice.h"
#include "HIDTypes.h"
#include <driver/adc.h>

// Axis X and Y center at 0
// SetAxis(AXIS_X, [-32767, 32767]);

// All other Axes go 0 -> 65535
// SetAxis(AXIS_Z, [0, 65535]);

// 4 hats indexed from zero
// SetHat(0-3, HIT_DIR_N);

// 128 buttons indexed at zero, true(1) or false(0)
// SetButton(0-127, 1-0);



#define AXIS_X 0
#define AXIS_Y 1
#define AXIS_Z 2
#define AXIS_Rx 3
#define AXIS_Ry 4
#define AXIS_Rz 5
#define AXIS_S0 6
#define AXIS_S1 7

#define HAT_DIR_N 0
#define HAT_DIR_NE 1
#define HAT_DIR_E 2
#define HAT_DIR_SE 3
#define HAT_DIR_S 4
#define HAT_DIR_SW 5
#define HAT_DIR_W 6
#define HAT_DIR_NW 7
#define HAT_DIR_C 8

uint8_t hatDir[9][4] = {
    {0, 0, 0, 0},
    {0, 0, 0, 1},
    {0, 0, 1, 0},
    {0, 0, 1, 1},
    {0, 1, 0, 0},
    {0, 1, 0, 1},
    {0, 1, 1, 0},
    {0, 1, 1, 1},
    {1, 0, 0, 0}};

BLEHIDDevice *hid;         //declare hid device
BLECharacteristic *input;  //Characteristic that inputs button values to devices
BLECharacteristic *output; //Characteristic that takes input from client

//Stores if the board is connected or not
bool connected = false;
#define CONNECTED_LED_INDICATOR_PIN 2

uint8_t inputValues[34];
long lastHIDUpdate = 0;

class MyCallbacks : public BLEServerCallbacks
{ //Class that does stuff when device disconects or connects
    void onConnect(BLEServer *pServer)
    {
        connected = true;
        Serial.println("Connected");
        BLE2902 *desc = (BLE2902 *)input->getDescriptorByUUID(BLEUUID((uint16_t)0x2902));
        desc->setNotifications(true);

        digitalWrite(CONNECTED_LED_INDICATOR_PIN, HIGH);
    }

    void onDisconnect(BLEServer *pServer)
    {
        connected = false;
        Serial.println("Disconnected");
        BLE2902 *desc = (BLE2902 *)input->getDescriptorByUUID(BLEUUID((uint16_t)0x2902));
        desc->setNotifications(false);

        digitalWrite(CONNECTED_LED_INDICATOR_PIN, LOW);
    }
};

class MyOutputCallbacks : public BLECharacteristicCallbacks
{
    void onWrite(BLECharacteristic *me)
    {
        uint8_t *value = (uint8_t *)(me->getValue().c_str());
        //ESP_LOGI(LOG_TAG, "special keys: %d", *value);
    }
};

void taskServer(void *)
{

    BLEDevice::init("RealRobots Controller");
    BLEServer *pServer = BLEDevice::createServer();
    pServer->setCallbacks(new MyCallbacks());

    hid = new BLEHIDDevice(pServer);
    input = hid->inputReport(1);   // <-- input REPORTID from report map
    output = hid->outputReport(1); // <-- output REPORTID from report map

    output->setCallbacks(new MyOutputCallbacks());

    std::string name = "REALROBOTS";
    hid->manufacturer()->setValue(name);

    hid->pnp(0x02, 0xe502, 0xa111, 0x0210);
    hid->hidInfo(0x00, 0x02);

    BLESecurity *pSecurity = new BLESecurity();
    //  pSecurity->setKeySize();
    pSecurity->setAuthenticationMode(ESP_LE_AUTH_BOND);

    // 128 buttons, no axes
    const uint8_t report[] = {
        //This is where the amount, type, and value range of the inputs are declared
        0x05, 0x01, // USAGE_PAGE (Generic Desktop)
        0x09, 0x04, // USAGE (Gamepad)
        0xa1, 0x01, // COLLECTION (Application)

        0x85, 0x01, //   REPORT_ID (1)

        0x05, 0x09, // USAGE_PAGE (Button)
        0x19, 0x01, // USAGE_MINIMUM (Button 1)
        0x29, 0x80, // USAGE_MAXIMUM (Button 128)
        0x15, 0x00, // LOGICAL_MINIMUM (0)
        0x25, 0x01, // LOGICAL_MAXIMUM (1)
        0x95, 0x80, // REPORT_COUNT (128)
        0x75, 0x01, // REPORT_SIZE (1)
        0x81, 0x02, // INPUT (Data,Var,Abs)

        0x05, 0x01, // USAGE_PAGE (Generic Desktop) // analog axes

        0x09, 0x30,       // USAGE (X)
        0x09, 0x31,       // USAGE (Y)
        0x16, 0x01, 0x80, //LOGICAL_MINIMUM (-32767)
        0x26, 0xFF, 0x7F, //LOGICAL_MAXIMUM (32767)
        0x75, 0x10,       //     REPORT_SIZE (16)
        0x95, 0x02,       // REPORT_COUNT (2)
        0x81, 0x02,       // INPUT (Data,Var,Abs)

        0x09, 0x39,     // USAGE (HATSWITCH)
        0x15, 0x00,     // LOGICAL_MINIMUM (0)
        0x25, 0x07,     // LOGICAL_MAXIMUM (7)
        0x35, 0x00,     // PHYSICAL_MINIMUM (0)
        0x46, 0x38, 01, // PHYSICAL_MAXIMUM (315)
        0x65, 0x14,     // UNIT (Eng Rot:Angular Pos)
        0x75, 0x04,     // REPORT_SIZE (4)
        0x95, 0x01,     // REPORT_COUNT (1)
        0x81, 0x02,     // INPUT(Data, Var, Abs)

        0x09, 0x39,     // USAGE (HATSWITCH)
        0x15, 0x00,     // LOGICAL_MINIMUM (0)
        0x25, 0x07,     // LOGICAL_MAXIMUM (7)
        0x35, 0x00,     // PHYSICAL_MINIMUM (0)
        0x46, 0x38, 01, // PHYSICAL_MAXIMUM (315)
        0x65, 0x14,     // UNIT (Eng Rot:Angular Pos)
        0x75, 0x04,     // REPORT_SIZE (4)
        0x95, 0x01,     // REPORT_COUNT (1)
        0x81, 0x02,     // INPUT(Data, Var, Abs)

        0x09, 0x39,     // USAGE (HATSWITCH)
        0x15, 0x00,     // LOGICAL_MINIMUM (0)
        0x25, 0x07,     // LOGICAL_MAXIMUM (7)
        0x35, 0x00,     // PHYSICAL_MINIMUM (0)
        0x46, 0x38, 01, // PHYSICAL_MAXIMUM (315)
        0x65, 0x14,     // UNIT (Eng Rot:Angular Pos)
        0x75, 0x04,     // REPORT_SIZE (4)
        0x95, 0x01,     // REPORT_COUNT (1)
        0x81, 0x02,     // INPUT(Data, Var, Abs)

        0x09, 0x39,     // USAGE (HATSWITCH)
        0x15, 0x00,     // LOGICAL_MINIMUM (0)
        0x25, 0x07,     // LOGICAL_MAXIMUM (7)
        0x35, 0x00,     // PHYSICAL_MINIMUM (0)
        0x46, 0x38, 01, // PHYSICAL_MAXIMUM (315)
        0x65, 0x14,     // UNIT (Eng Rot:Angular Pos)
        0x75, 0x04,     // REPORT_SIZE (4)
        0x95, 0x01,     // REPORT_COUNT (1)
        0x81, 0x02,     // INPUT(Data, Var, Abs)

        0x09, 0x32,             // USAGE (Z)
        0x09, 0x33,             // USAGE (Rx)
        0x09, 0x34,             // USAGE (Ry)
        0x09, 0x35,             // USAGE (Rz)   //36-37=steering s0
        0x09, 0x36,             //     USAGE (Slider) Slider 1
        0x09, 0x36,             //     USAGE (Slider) Slider 2
        0x16, 0x01, 0x80, //LOGICAL_MINIMUM (-32767)
        0x26, 0xFF, 0x7F, //LOGICAL_MAXIMUM (32767)
        0x75, 0x10,             //     REPORT_SIZE (16)
        0x95, 0x06,             //     REPORT_COUNT (6)
        0x81, 0x02,             //     INPUT (Data, Variable, Absolute) ;20 bytes (slider 1 and slider 2)
        0xc0                    // END_COLLECTION
    };

    hid->reportMap((uint8_t *)report, sizeof(report));
    hid->startServices();

    BLEAdvertising *pAdvertising = pServer->getAdvertising();
    pAdvertising->setAppearance(HID_GAMEPAD);
    pAdvertising->addServiceUUID(hid->hidService()->getUUID());
    pAdvertising->start();
    hid->setBatteryLevel(99);

    ESP_LOGD(LOG_TAG, "Advertising started!");
    delay(portMAX_DELAY);
};

void SetButton(int idx, bool val)
{
    if (idx > 128 || idx < 0)
    {
        return;
    }
    bitWrite(inputValues[idx / 8], idx % 8, val);
}

void SetHat(uint8_t hatIdx, uint8_t dir)
{
    switch (hatIdx)
    {
    case 0:
        for (int i = 0; i < 4; i++)
        {
            bitWrite(inputValues[20], 3 - i, hatDir[dir][i]);
        }
        break;
    case 1:
        for (int i = 0; i < 4; i++)
        {
            bitWrite(inputValues[20], 7 - i, hatDir[dir][i]);
        }
        break;
    case 2:
        for (int i = 0; i < 4; i++)
        {
            bitWrite(inputValues[21], 3 - i, hatDir[dir][i]);
        }
        break;
    case 3:
        for (int i = 0; i < 4; i++)
        {
            bitWrite(inputValues[21], 7 - i, hatDir[dir][i]);
        }
        break;
    }
}

void ZeroHats()
{
    for (int i = 0; i < 3; i++)
    {
        SetHat(i, HAT_DIR_C);
    }
}

void SetAxis(uint8_t axis, int setVal)
{
    switch (axis)
    {
    case 0: // X Axis
        inputValues[16] = lowByte(setVal);
        inputValues[17] = highByte(setVal);
        break;
    case 1: // Y Axis
        inputValues[18] = lowByte(setVal);
        inputValues[19] = highByte(setVal);
        break;
    case 2: // Z Axis
        inputValues[22] = lowByte(setVal);
        inputValues[23] = highByte(setVal);
        break;
    case 3: // Rx Axis
        inputValues[24] = lowByte(setVal);
        inputValues[25] = highByte(setVal);
        break;
    case 4: // Ry Axis
        inputValues[26] = lowByte(setVal);
        inputValues[27] = highByte(setVal);
        break;
    case 5: // Rz Axis
        inputValues[28] = lowByte(setVal);
        inputValues[29] = highByte(setVal);
        break;
    case 6: // S0 Axis
        inputValues[32] = lowByte(setVal);
        inputValues[33] = highByte(setVal);
        break;
    case 7: // S1 Axis
        inputValues[30] = lowByte(setVal);
        inputValues[31] = highByte(setVal);
        break;
    }
}

void initBLEDevice()
{
    //Serial.begin(115200);

    for (int i = 0; i < 17; i++)
    {
        inputValues[i] = 0b00000000;
    }
    ZeroHats();

    xTaskCreate(taskServer, "server", 20000, NULL, 5, NULL);
}



void updateHIDDevice()
{

    if (connected && millis() - lastHIDUpdate > 20)
    {
        input->setValue(inputValues, sizeof(inputValues));
        input->notify();
        lastHIDUpdate = millis();
        //delay(20);
    }
}