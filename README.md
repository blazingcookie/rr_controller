# RealRobots Controller firmware

## What is it?

Upload this firmware to an Arduino Pro Micro for USB HID or ESP32 for Bluetooth HID
and then configure it using the [RealRobots Configurator](https://www.gitlab.com/realrobots/rr_configurator.git) 
App to have it act as an HID game controller and 
translate any buttons, potentiometers etc as game controller inputs.

Devices with this firmware communicate with each other through i2c and report HID through the master device, 
build an entire cockpit on a single USB and configure it through one app and never touch a line of code.


## Dependencies

USB HID makes use of the fantastic [ArduinoJoystickLibrary](https://github.com/MHeironimus/ArduinoJoystickLibrary) from MHeironimus.


##### Get full DIY kits, ready to assemble from RealRobots [here](http://128.199.98.192/store).
![](./pic2_xs.jpg)


#### Support free and open code by contributing on [patreon.com/realrobots](https://www.patreon.com/realrobots)
